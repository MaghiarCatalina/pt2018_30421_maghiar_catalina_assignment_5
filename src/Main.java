import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public List<MonitoredData> lista = new ArrayList<>();
    //Task 0
    public void readData() {
        try (Stream<String> stream = Files.lines(Paths.get("Activities.txt"))) {
                stream.forEach(p->

                {   String fields[] = p.split("\t\t"); //separate a line from the file after two tabs
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    LocalDateTime start = LocalDateTime.parse(fields[0], formatter);
                    LocalDateTime end = LocalDateTime.parse(fields[1], formatter);
                    MonitoredData aux = new MonitoredData(start,end,fields[2]); //create an object with the values from each line
                    lista.add(aux); // add the object to the list
                });
        } catch (Exception e) {
                e.printStackTrace();
        }
    }
    // Task 1: Count how many distinct days

    public int distinctDaysCount(){
        //group the data by the start time day and count how many with size()
       return lista.stream().collect(Collectors.groupingBy(p->p.getStart_time().getDayOfMonth())).size();
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T,Object> keyExtractor) {
        Map<Object,String> seen = new ConcurrentHashMap<>();
        return t -> seen.put(keyExtractor.apply(t), "") == null;
    }

    //Task 2: Map: distinct action -> number of occurrences
    public Map<String ,Long> nrOfOccurencesEachAction(){
        Map<String ,Long> result;   //what will be returned
        List<String> act = lista.stream().map(MonitoredData::getActivity).collect(Collectors.toList()); //activities
        result = act.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting())); //making the map
        List<String> content= new ArrayList<>();
        try {
            Path file = Paths.get("OccurrencesOfAnActivity.txt");
            for(Map.Entry<String ,Long> entry: result.entrySet()){
                //for each map entry, put it to string and write into file
                content.add(entry.getKey()+"  "+entry.getValue().toString());
                Files.write(file, content, Charset.forName("UTF-8"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    return result;
    }

    public void checking(Map<String,Long> x){
       for(Map.Entry<String,Long> e: x.entrySet()){
           System.out.println(e.getKey().toString()+"   "+e.getValue().toString());
       }
    }

    public static void main(String args[]){
            Main m = new Main();
            m.readData();
            System.out.println("Count the distinct days that appear in the monitoring data:");
            System.out.println(m.distinctDaysCount());

        // m.checking(m.nrOfOccurencesEachAction()); //files already created, just open them to see the result

    }
}
