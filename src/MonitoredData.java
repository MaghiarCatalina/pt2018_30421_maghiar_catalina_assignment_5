import java.time.LocalDateTime;

public class MonitoredData {
    public LocalDateTime start_time;
    public LocalDateTime end_time;
    public String activity;
    public MonitoredData(){}
    public MonitoredData(LocalDateTime start, LocalDateTime end, String activity){
        this.start_time=start;
        this.end_time=end;
        this.activity=activity;
    }

    public String getActivity() {
        return activity;
    }

    public LocalDateTime getEnd_time() {
        return end_time;
    }

    public LocalDateTime getStart_time() {
        return start_time;
    }

    @Override
    public String toString() {
        return start_time.toString()+"   "+end_time.toString()+"   "+activity;
    }
}
